package ua.nure.yaritenko.Practice8.db;

import ua.nure.yaritenko.Practice8.db.entity.Group;
import ua.nure.yaritenko.Practice8.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private static DBManager instance;

    private DBManager() {
    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private static final String CONNECTION_URL = "jdbc:mysql://localhost/myDb" + "?user=root&password=pass";

    public void insertUser(User user) throws SQLException {
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            String sqlAdd = "INSERT INTO users(id, login) VALUES(default, ?)";
            prstmt = con.prepareStatement(sqlAdd, Statement.RETURN_GENERATED_KEYS);
            prstmt.setString(1, user.getUser());
            if (prstmt.executeUpdate() > 0) {
                rs = prstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId_user(rs.getInt(1));
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
    }

    public void insertGroup(Group group) throws SQLException {
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            String sqlAdd = "INSERT INTO groups(id, name) VALUES(default, ?)";
            prstmt = con.prepareStatement(sqlAdd, Statement.RETURN_GENERATED_KEYS);
            prstmt.setString(1, group.getName());
            if (prstmt.executeUpdate() > 0) {
                rs = prstmt.getGeneratedKeys();
                if (rs.next()) {
                    group.setId_gpoup(rs.getInt(1));
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
    }


    public List <User> findAllUsers() throws SQLException {
        List <User> listUser = new ArrayList <>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM users ORDER BY id");
            while (rs.next()) {
                User user = extractUser(rs);
                listUser.add(user);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(stmt);
            closeConnection(con);
        }
        return listUser;
    }

    public List <Group> findAllGroups() throws SQLException {
        List <Group> listGroup = new ArrayList <>();
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM groups ORDER BY id");
            while (rs.next()) {
                Group group = extractGroup(rs);
                listGroup.add(group);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(stmt);
            closeConnection(con);
        }
        return listGroup;
    }

    private Group extractGroup(ResultSet rs) throws SQLException {
        Group group = new Group();
        group.setId_gpoup(rs.getInt("id"));
        group.setName(rs.getString("name"));
        return group;
    }


    private User extractUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId_user(rs.getInt(1));
        user.setUser(rs.getString(2));
        return user;
    }

    public User getUser(String nameUser) throws SQLException {
        User user = new User();
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            prstmt = con.prepareStatement("SELECT * FROM users WHERE login = ?");
            prstmt.setString(1, nameUser);
            rs = prstmt.executeQuery();
            while (rs.next()) {
                user.setId_user(rs.getInt("id"));
                user.setUser(rs.getString("login"));
            }
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
        return user;
    }

    public Group getGroup(String team) throws SQLException {
        Group group = new Group();
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            prstmt = con.prepareStatement("SELECT * FROM groups WHERE name = ?");
            prstmt.setString(1, team);
            rs = prstmt.executeQuery();
            while (rs.next()) {
                /*group.setId_gpoup(rs.getInt("id"));
                group.setName(rs.getString("name"));*/
                group = extractGroup(rs);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
        return group;
    }

    public void setGroupsForUser(User user, Group... groups) throws SQLException {
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            con.setAutoCommit(false);
            for (Group group : groups) {
                try {
                    prstmt = con.prepareStatement("INSERT INTO users_groups(user_id, group_id) VALUES (?, ?)");
                    prstmt.setInt(1, user.getId_user());
                    prstmt.setInt(2, group.getId_gpoup());
                    prstmt.executeUpdate();
                    con.commit();
                } catch (SQLException ex) {
                    con.rollback();
                    System.out.println("Произведен откат транзакции:" + ex.getMessage());
                }
            }
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
    }

    public List <Group> getUserGroups(User user) throws SQLException {
        List <Group> listUserGroup = new ArrayList <>();
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            prstmt = con.prepareStatement("select u.id, g.name from groups g, users_groups ug, users u where g.id IN (select ug.group_id from users_groups where ug.user_id = u.id and u.id = ?)");
            prstmt.setInt(1, user.getId_user());
            rs = prstmt.executeQuery();
            while (rs.next()) {
                Group group = extractGroup(rs);
                listUserGroup.add(group);
            }
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
        return listUserGroup;
    }

    public void deleteGroup(Group group) throws SQLException {
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            prstmt = con.prepareStatement("delete from groups where name = ?");
            prstmt.setString(1, group.getName());
            prstmt.executeUpdate();
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
    }

    public void updateGroup(Group teamC) throws SQLException {
        Connection con = null;
        PreparedStatement prstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            prstmt = con.prepareStatement("UPDATE groups SET name = ? WHERE id = ?");
            prstmt.setString(1, teamC.getName());
            prstmt.setInt(2, teamC.getId_gpoup());
            prstmt.executeUpdate();
        } finally {
            closeResultSet(rs);
            closeStatement(prstmt);
            closeConnection(con);
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(CONNECTION_URL);
    }

    private void closeConnection(Connection con) throws SQLException {
        if (con != null) {
            con.close();
        }
    }

    private void closeStatement(Statement st) throws SQLException {
        if (st != null) {
            st.close();
        }
    }

    private void closeResultSet(ResultSet rs) throws SQLException {
        if (rs != null) {
            rs.close();
        }
    }
}
