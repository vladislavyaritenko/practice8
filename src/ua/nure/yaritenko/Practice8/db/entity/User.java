package ua.nure.yaritenko.Practice8.db.entity;

import java.util.ArrayList;

public class User {
    private String user;
    private int id_user;


    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public static User createUser(String newUser) {
        User user = new User();
        user.setUser(newUser);
        return user;
    }

    public void setUser(String newUser) {
        this.user = newUser;
    }

    public String getUser() {
        return user;
    }


    @Override
    public String toString() {
        return "User [id=" + getId_user() + ", login=" + getUser() + "]";
    }
}
