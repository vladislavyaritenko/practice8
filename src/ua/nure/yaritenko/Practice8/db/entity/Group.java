package ua.nure.yaritenko.Practice8.db.entity;

public class Group {
    String name;
    int id_gpoup;

    public static Group createGroup(String newGroup){
        Group group = new Group();
        group.setName(newGroup);
        return group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId_gpoup() {
        return id_gpoup;
    }

    public void setId_gpoup(int id_gpoup) {
        this.id_gpoup = id_gpoup;
    }
    @Override
    public String toString() {
        return "Group [id=" + getId_gpoup() + ", name=" + getName() + "]";
    }


}
