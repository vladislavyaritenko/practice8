DROP DATABASE myBD;

CREATE DATABASE myDB;
USE myDB;
CREATE TABLE users(id INT PRIMARY KEY NOT NULL  AUTO_INCREMENT,
                  login VARCHAR(40)UNIQUE NOT NULL);

CREATE TABLE groups(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                  name VARCHAR(40) NOT NULL UNIQUE);

CREATE TABLE users_groups(user_id INT REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
                          group_id INT REFERENCES groups(id) ON UPDATE CASCADE ON DELETE CASCADE,
                          PRIMARY KEY(user_id, group_id));

INSERT INTO users (id, login) VALUES (DEFAULT, 'ivanov');
INSERT INTO groups (id, name) VALUES (DEFAULT, 'teamA');